﻿namespace fhir_client_csharp
{
    class Program
    {

        static void Main(string[] _)
        {
            Example.Ex00_TheRawWay();
            // Example.Ex01_SimpleRead();
            // Example.Ex02_ReadWithSettings();
            // Example.Ex03_ReadWithLogging();
            // Example.Ex04_ErrorHandling();
            // Example.Ex05_ReadPatient42();
            // Example.Ex06_CapabilityStatement();
            // Example.Ex07_CreatePatientWithRandomIdentifier();
            // Example.Ex08_SearchPatients();
            // Example.Ex09_SearchWithError();
            // Example.Ex10_SearchAndBarfOnError();
            // Example.Ex11_Authorization();
        }


    }
}
