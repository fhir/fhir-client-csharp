using System;
using System.IO;
using System.Linq;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Hl7.Fhir.Serialization;
using T = System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace fhir_client_csharp
{
    public static class Server
    {
        public const string VonkR4 = "https://vonk.fire.ly/R4";
        public const string HapiR4 = "http://hapi.fhir.org/baseR4";
        public const string FhirTestR4 = "http://test.fhir.org/r4";
    }
    public static class Example
    {
        public static void Ex00_TheRawWay()
        {
            var httpClient = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, $"{Server.VonkR4}/Patient/42");
            var fhirJsonMediaType = MediaTypeWithQualityHeaderValue.Parse("application/fhir+json; charset=UTF-8; fhirVersion=4.0");
            request.Headers.Accept.Add(fhirJsonMediaType);

            var response = httpClient.SendAsync(request)
                .GetAwaiter().GetResult();
            var json = Encoding.UTF8.GetString(response.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult());
            var patient42 = new FhirJsonParser().Parse<Patient>(json);

            var patientName = patient42.Name.First();
            Console.WriteLine($"Patient name is {patientName.Given.First()} {patientName.Family}");
        }

        public static void Ex01_SimpleRead()
        {
            var fhirClient = new FhirClient(Server.VonkR4);

            var patient42 = fhirClient.Read<Patient>("Patient/42");
            var patientName = patient42.Name.First();
            Console.WriteLine($"Patient name is {patientName.Given.First()} {patientName.Family}");
        }

        /// <summary>
        /// Change some settings.
        /// e.g. Json is not the default (that is XML)
        /// </summary>
        public static void Ex02_ReadWithSettings()
        {
            var settings = new FhirClientSettings
            {
                CompressRequestBody = true,
                PreferredReturn = Prefer.ReturnRepresentation,
                PreferredFormat = ResourceFormat.Json
                // Other settings to investigate:
                // ,ParserSettings
                // ,PreferCompressedResponses
                // ,PreferredParameterHandling
                // ,Timeout
                // ,UseFormatParameter
                // ,VerifyFhirVersion
            };
            var fhirClient = new FhirClient(Server.VonkR4, settings);
            var patient42 = fhirClient.Read<Patient>("Patient/42");
        }

        public static void Ex03_ReadWithLogging()
        {
            var loggingHandler = new LoggingHttpMessageHandler();
            var fhirClient = new FhirClient(Server.VonkR4, settings: null, messageHandler: loggingHandler);
            var patient42 = fhirClient.Read<Patient>("Patient/42");
        }

        public static void Ex04_ErrorHandling()
        {
            // try
            // {
            var loggingHandler = new LoggingHttpMessageHandler();
            var fhirClient = new FhirClient(Server.VonkR4, settings: null, messageHandler: loggingHandler);
            var unknownId = Guid.NewGuid().ToString();
            var unknownPatient = fhirClient.Read<Patient>($"Patient/{unknownId}");
            // }
            // catch (Exception ex)
            // {
            //     Console.WriteLine($"Error: {ex.Message}");
            // }

        }

        public static void Ex05_Prepare_CreateOrUpdate42()
        {
            var fhirClient = Client.Complete(Server.VonkR4);
            try
            {
                var patient42Json = File.ReadAllText("./data/Patient42.json");
                var patient42 = new FhirJsonParser().Parse<Patient>(patient42Json);
                var patient42Saved = fhirClient.Update<Patient>(patient42);
                var patientName = patient42Saved.Name.First();
                Console.WriteLine($"Patient42 is saved with id {patient42Saved.Id}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public static void Ex05_ReadPatient42()
        {
            var fhirClient = Client.Complete(Server.VonkR4);
            try
            {
                var patient42 = fhirClient.Read<Patient>("Patient/42");
                var patientName = patient42.Name.First();
                Console.WriteLine($"Patient name is {patientName.Given.First()} {patientName.Family}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public static void Ex06_CapabilityStatement()
        {
            var fhirClient = Client.Complete(Server.VonkR4);
            try
            {
                var capabilityStatement = fhirClient.CapabilityStatement();
                var output = capabilityStatement.ToJson(new FhirJsonSerializationSettings { Pretty = true });
                Console.WriteLine($"The server at {fhirClient.Endpoint} returned these capabilities:");
                Console.WriteLine(output.Substring(0, 100) + " [...]");

                //Now we will ask the server how many resources it has for every resourcetype in the CapabilityStatement.
                //It shows how to read parts of the CapabilityStatement.
                var supportedTypes = capabilityStatement.Rest[0].Resource.Select(res => res.Type).ToList();
                foreach (var resourceType in supportedTypes.Take(5))
                {
                    if (resourceType.HasValue)
                    {
                        var resourceTypeName = ModelInfo.ResourceTypeToFhirTypeName(resourceType.Value);
                        var foundOfThisType = fhirClient.Search(new SearchParams { Count = 0 }, resourceTypeName);
                        var nrOfInstances = foundOfThisType.Total;
                        Console.WriteLine($"# of {resourceType} resources: {nrOfInstances.ToString() ?? "unknown"}");
                    }
                    else
                    {
                        Console.WriteLine("Null resourcetype encountered - that is unexpected");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public static void Ex07_CreatePatientWithRandomIdentifier()
        {
            var fhirClient = Client.Complete(Server.VonkR4);
            try
            {
                var patient = new Patient();
                var identifierValue = Guid.NewGuid().ToString();
                patient.Identifier.Add(
                    new Identifier(
                        system: "http://vogon.space/earth",
                        value: identifierValue));
                var createdPatient = fhirClient.Create(patient);
                Console.WriteLine($"Patient with identifier {identifierValue} is saved with id {createdPatient.Id}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public static void Ex08_SearchPatients()
        {
            var fhirClient = Client.Complete(Server.VonkR4);
            try
            {
                var searchParams = new SearchParams();
                var identifier = "http://vogon.space/earthlings|2";
                searchParams.Add("identifier", identifier);
                var matchingPatients = fhirClient.Search(searchParams, "Patient");

                Console.WriteLine($"# of Patients with identifier {identifier} is {matchingPatients.Total}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public static void Ex09_SearchWithError()
        {
            var fhirClient = Client.Complete(Server.VonkR4);
            try
            {
                var searchParams = new SearchParams();
                var identifier = "http://vogon.space/earthlings|2";
                searchParams.Add("ident", identifier); //ident is not a valid searchparameter
                var matchingPatients = fhirClient.Search(searchParams, "Patient");

                var operationOutcome = matchingPatients.Entry.FirstOrDefault(e => e.Resource is OperationOutcome)?.Resource as OperationOutcome;
                if (operationOutcome is OperationOutcome)
                {
                    Console.WriteLine("Errors were returned in the search:");
                    foreach (var issue in operationOutcome.Issue)
                    {
                        Console.WriteLine(issue.Details?.Text ?? issue.Diagnostics);
                    }
                }

                Console.WriteLine($"# of Patients found is {matchingPatients.Total}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public static void Ex10_SearchAndBarfOnError()
        {
            var fhirClient = Client.Complete(Server.VonkR4);
            fhirClient.Settings.PreferredParameterHandling = SearchParameterHandling.Strict;
            //This setting is sadly not yet evaluated by Vonk, so we get the same result as in Ex90.
            try
            {
                var searchParams = new SearchParams();
                var identifier = "http://vogon.space/earthlings|2";
                searchParams.Add("ident", identifier); //ident is not a valid searchparameter
                var matchingPatients = fhirClient.Search(searchParams, "Patient");

                Console.WriteLine($"# of Patients found is {matchingPatients.Total}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public static void Ex11_Authorization()
        {
            var authHandler = new AuthorizationMessageHandler();
            authHandler.Authorization = AuthenticationHeaderValue.Parse("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");
            var fhirClient = new FhirClient(Server.VonkR4, settings:null, messageHandler: authHandler );
            fhirClient.Read<Patient>("Patient/42");
        }
    }
}