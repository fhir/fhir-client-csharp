using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Xml;
using Hl7.Fhir.Rest;
using Newtonsoft.Json.Linq;

internal class LoggingHttpMessageHandler : HttpClientEventHandler
    {
        protected override void BeforeRequest(HttpRequestMessage rawRequest, byte[] body)
        {
            var sb = new StringBuilder();
            sb.Append(rawRequest.Method).Append(" ")
                .AppendLine(rawRequest.RequestUri.ToString());
            sb.AppendLine("{");
            foreach (var header in rawRequest.Headers)
            {
                var value = string.Join(' ', header.Value);
                sb.Append("  ").Append(header.Key).Append("=").AppendLine(value);
            }
            sb.AppendLine("}");

            if (body.Length > 0)
            {
                var raw = Encoding.UTF8.GetString(body);
                var pretty = Prettify(raw);
                sb.AppendLine("### body ###");
                sb.Append("  ").AppendLine(pretty);
                sb.AppendLine("### end of body ###");
            }
            else
            {
                sb.Append("### no body ###");
            }
            Console.WriteLine(sb.ToString());
        }
        protected override void AfterResponse(HttpResponseMessage webResponse, byte[] body)
        {
            var sb = new StringBuilder();
            sb.Append("StatusCode: ").AppendLine(webResponse.StatusCode.ToString());
            sb.AppendLine("{");
            foreach (var header in webResponse.Headers)
            {
                var value = string.Join(' ', header.Value);
                sb.Append("  ").Append(header.Key).Append("=").AppendLine(value);
            }
            sb.AppendLine("}");

            if (body.Length > 0)
            {
                var raw = Encoding.UTF8.GetString(body);
                var pretty = Prettify(raw);
                sb.AppendLine("### body ###");
                sb.AppendLine(pretty);
                sb.AppendLine("### end of body ###");
            }
            else
            {
                sb.Append("### no body ###");
            }
            Console.WriteLine(sb.ToString());
        }

        private string Prettify(string raw)
        {
            if (raw.StartsWith("{")) //this is json
            {
                return JValue.Parse(raw).ToString(Newtonsoft.Json.Formatting.Indented);
            }
            if (raw.StartsWith("<")) // this is XML
            {
                var sw = new StringWriter();
                var xw = new XmlTextWriter(sw);
                xw.Formatting = System.Xml.Formatting.Indented;
                xw.Indentation = 4;
                var doc = new XmlDocument();
                doc.LoadXml(raw);
                doc.Save(xw);
                return sw.ToString();
            }
            return raw;
        }
    }
