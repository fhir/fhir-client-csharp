using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Xml;
using Hl7.Fhir.Rest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fhir_client_csharp
{
    public static class Client
    {
        public static FhirClient Bare(string fhirServerAddress)
        {
            return new FhirClient(fhirServerAddress);
        }

        public static FhirClient WithSettings(string fhirServerAddress)
        {
            var clientSettings = new FhirClientSettings
            {
                CompressRequestBody = true,
                PreferredReturn = Prefer.ReturnRepresentation,
                PreferredFormat = ResourceFormat.Json
                // Other settings to investigate:
                // ,ParserSettings
                // ,PreferCompressedResponses
                // ,PreferredParameterHandling
                // ,Timeout
                // ,UseFormatParameter
                // ,VerifyFhirVersion
            };
            return new FhirClient(fhirServerAddress, clientSettings);
        }

        /// <summary>
        /// A FhirClient with a custom HttpMessageHandler attached.
        /// Such a handler allows you to inspect or alter the raw http message 
        /// before it is sent and after it returns.
        /// In this example the request and the response is logged to the Console.
        /// </summary>
        public static FhirClient WithLogging(string fhirServerAddress)
        {
            var loggingHandler = new LoggingHttpMessageHandler();
            return new FhirClient(fhirServerAddress, settings: null, messageHandler: loggingHandler);
        }

        public static FhirClient Complete(string fhirServerAddress)
        {
            var loggingHandler = new LoggingHttpMessageHandler();
            var settings = new FhirClientSettings
            {
                CompressRequestBody = true,
                PreferredReturn = Prefer.ReturnRepresentation,
                PreferredFormat = ResourceFormat.Json
                // Other settings to investigate:
                // ,ParserSettings
                // ,PreferCompressedResponses
                // ,PreferredParameterHandling
                // ,Timeout
                // ,UseFormatParameter
                // ,VerifyFhirVersion
            };
            return new FhirClient(fhirServerAddress, settings, loggingHandler);
            // note: you can assign the Settings after creating the FhirClient like below,
            // but they are not evaluated then.
            // fhirClient.Settings = new settings;
        }
    }

}
